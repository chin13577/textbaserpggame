﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class ItemPerk
    {
        public string perkName;
        //Buffs
        public int atk;
        public int def;
        public int hp;
        public int mp;
        public int buffStrength;
        public int buffPerception;
        public int buffEndurance;
        public int buffCharisma;
        public int buffIntelligence;
        public int buffAgility;
        public int buffLuck;
        
        public ItemPerk(string name)
        {
            perkName = name;
        }
    }
}
