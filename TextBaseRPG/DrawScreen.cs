﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class DrawScreen
    {
        public static string DrawCharacter(string npcName)
        {
            if (npcName == "Chinnie")
            {
                return " _/-\\_\n(o'_')\n / | \\ \n _/-\\_\n";
            }
            else
                return "";
        }
        /// <summary>
        /// Draw player's inventory screen.
        /// </summary>
        /// <param name="player">player</param>
        public static void DrawInventoryScreen(Player player)
        {
            Console.Clear();
            string items = "";
            Console.WriteLine("### Inventory ###\n");
            Console.WriteLine("------------------------");
            Console.WriteLine("Inventory : " + player.Inventory.Count + "/" + player.maxInventory);
            Console.WriteLine("Carry Weight : " + player.carryWeight + "/" + player.maxCarryWeight);
            Console.WriteLine("Equip Weight : " + player.equipWeight + "/" + player.maxEquipWeight);
            Console.WriteLine("------------------------");
            int num = 1;
            foreach (ItemInventory itemInv in player.Inventory)
            {
                items += (num++) + ") " + (itemInv.item.name + " x" + itemInv.count + "\n");
            }

            if (items == "")
                Console.WriteLine("empty");
            else
                Console.WriteLine(items);

            Console.WriteLine("\nPress Command button continue.");
            Console.WriteLine("\n(Backspace) to continue.");
            Console.WriteLine("(D)Detail of Item");
            Console.WriteLine("(E)Use/Equip Item");
            Console.WriteLine("(R)Remove Item");
        }
        /// <summary>
        /// draw player's status screen.
        /// </summary>
        /// <param name="player"></param>
        public static void DrawStatusScreen(Player player)
        {
            Console.Clear();
            Console.WriteLine("### Status ###\n");
            Console.WriteLine("HP : " + player.curHp + "/" + player.maxHp);
            Console.WriteLine("MP : " + player.curMp + "/" + player.maxMp);
            Console.WriteLine("------------------------");
            Console.WriteLine("Carry Weight : " + player.carryWeight + "/" + player.maxCarryWeight);
            Console.WriteLine("Equip Weight : " + player.equipWeight + "/" + player.maxEquipWeight);
            Console.WriteLine("------------------------");
            Console.WriteLine("Attack : " + player.atk);
            Console.WriteLine("Defence : " + player.def);
            Console.WriteLine("------------------------");
            Console.WriteLine("Strength : " + player.GetTotalStrength());
            Console.WriteLine("Endurance : " + player.GetTotalEndurance());
            Console.WriteLine("Perception : " + player.GetTotalPerception());
            Console.WriteLine("Intelligence : " + player.GetTotalIntelligence());
            Console.WriteLine("Agility : " + player.GetTotalAgility());
            Console.WriteLine("Charisma : " + player.GetTotalCharisma());
            Console.WriteLine("Luck : " + player.GetTotalLuck());
            Console.WriteLine("------------------------");
            Console.WriteLine("Perk :");
            for (int i = 0; i < player.bonusStat.perks.Count; i++)
            {
                Console.WriteLine(player.bonusStat.perks[i]);
            }
            Console.WriteLine("\nPress Command button continue.");
            Console.WriteLine("\n(Backspace) to continue.");
        }
        /// <summary>
        /// draw player's equipment screen.
        /// </summary>
        /// <param name="player"></param>
        public static void DrawEquipmentScreen(Player player)
        {
            Console.Clear();
            Console.WriteLine("### Equipment ###");
            Console.WriteLine("\n Accessory \n-----------------");
            Console.WriteLine("Neck : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Neck].name);
            Console.WriteLine("Cape : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Cape].name);
            Console.WriteLine("Left ear : {0}", player.equipment.accessories[(int)Equipment.Accessorys.L_ear].name);
            Console.WriteLine("Right ear : {0}", player.equipment.accessories[(int)Equipment.Accessorys.R_ear].name);
            Console.WriteLine("Ring1 : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Ring1].name);
            Console.WriteLine("Ring2 : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Ring2].name);
            Console.WriteLine("Ring3 : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Ring3].name);
            Console.WriteLine("Belt : {0}", player.equipment.accessories[(int)Equipment.Accessorys.Belt].name);
            Console.WriteLine("\n Weapons \n-----------------");
            Console.WriteLine("Left Hand : {0}\nRight Hand : {1}", player.equipment.weapons[0].name, player.equipment.weapons[1].name);
            Console.WriteLine("\n Armors \n-----------------");
            Console.WriteLine("Head : {0}", player.equipment.armors[(int)Equipment.Armors.Head].name);
            Console.WriteLine("Body : {0}", player.equipment.armors[(int)Equipment.Armors.Body].name);
            Console.WriteLine("Left Hand : {0}", player.equipment.armors[(int)Equipment.Armors.L_hand].name);
            Console.WriteLine("Right Hand : {0}", player.equipment.armors[(int)Equipment.Armors.R_hand].name);
            Console.WriteLine("Left Leg : {0}", player.equipment.armors[(int)Equipment.Armors.L_leg].name);
            Console.WriteLine("Right Leg : {0}", player.equipment.armors[(int)Equipment.Armors.R_leg].name);

            Console.WriteLine("\nPress Command button continue.");
            Console.WriteLine("\n(Backspace) to continue.");
            Console.WriteLine("(S)Select Item");
        }
        /// <summary>
        /// draw open chest screen.
        /// </summary>
        /// <param name="ListOfChest"></param>
        /// <param name="id"></param>
        public static void DrawOpenChestScreen(List<List<ItemInventory>> ListOfChest, int id)
        {
            Console.Clear();
            Console.WriteLine("### Chest {0} ###\n", id);
            string itemList = "";
            for (int i = 0; i < ListOfChest[id].Count; i++)
            {
                itemList += (i + 1) + ") " + ListOfChest[id][i].item.name + " x" + ListOfChest[id][i].count + "\n";
            }
            Console.WriteLine(itemList + "\n");
            Console.WriteLine("\nPress Command button continue.");
            Console.WriteLine("\n(Backspace) to continue.");
            Console.WriteLine("(D)Detail of Item");
            Console.WriteLine("(P)Pickup Item");

        }
        /// <summary>
        /// draw the detail of item screen.
        /// </summary>
        /// <param name="item">item</param>
        public static void DrawDetailOfItemScreen(Item item)
        {
            Console.Clear();
            Console.WriteLine("### {0} ###", item.name);
            string detail = "";
            detail += item.CheckAvaliableStrData("Type", item.itemType.ToString());
            detail += item.CheckAvaliableFloatData("Weight", item.weight);
            detail += item.CheckAvaliableIntData("Price", item.price);
            detail += item.CheckAvaliableStrData("Size", item.size.ToString());

            if (item.itemType == Item.ItemType.Weapon)
            {
                detail += item.CheckAvaliableStrData("Weapon Type", item.weaponType.ToString());
                detail += item.CheckAvaliableStrData("Handed Type", item.handedType.ToString());
            }
            else if (item.itemType == Item.ItemType.Armor)
                detail += item.CheckAvaliableStrData("Armor Type", item.armorType.ToString());

            detail += item.CheckAvaliableIntData("Hp", item.hp);
            detail += item.CheckAvaliableIntData("Mp", item.mp);
            detail += item.CheckAvaliableIntData("Attack", item.atk);
            detail += item.CheckAvaliableIntData("Defense", item.def);
            detail += item.CheckAvaliableIntData("Strength", item.buffStrength);
            detail += item.CheckAvaliableIntData("Endurance", item.buffEndurance);
            detail += item.CheckAvaliableIntData("Agility", item.buffAgility);
            detail += item.CheckAvaliableIntData("Intelligence", item.buffIntelligence);
            detail += item.CheckAvaliableIntData("Perception", item.buffPerception);
            detail += item.CheckAvaliableIntData("Luck", item.buffLuck);
            detail += item.CheckAvaliableIntData("Charisma", item.buffCharisma);

            detail += "\n" + item.CheckAvaliableStrData("Detail", item.detail);
            Console.WriteLine(detail);
            Console.WriteLine("\nPress Command button continue.");
            Console.WriteLine("\n(Backspace) to continue.");
        }
        /// <summary>
        /// draw the detail of item screen.
        /// </summary>
        /// <param name="item">item</param>
        public static void DrawTalkWithNpc(string name)
        {
            Console.Clear();
            Console.WriteLine("\nNPC : {0}", name);
            Console.Write(DrawCharacter(name));
            Console.WriteLine("-----------------\n");
        }
    }
}