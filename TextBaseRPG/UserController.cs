﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class UserController
    {
        public Player player;
        public Map map = new Map();
        /// <summary>
        /// Get Input from user in Main Screen.
        /// </summary>
        public void GetCommandInput()
        {
            for (int i = 0; i < 10; i++)
                Console.Write("#");
            Console.Write("CONTROLLER");
            for (int i = 0; i < 10; i++)
                Console.Write("#");
            Console.WriteLine("\n\nPlease enter the command you'd like to execute: \n");
            Console.WriteLine("(Arrow Button)" + "Walk");
            Console.WriteLine("(S)Status \n(I)Inventory \n(E)Equipment");
            bool isFoundChest = false;
            bool isFoundDoor = false;
            bool isFoundNpc = false;
            if (map.FindObjectNearPlayer("c", map.playerPosition))
            {
                Console.WriteLine("(F)Open Chest");
                isFoundChest = true;
            }
            if (map.FindObjectNearPlayer("d", map.playerPosition))
            {
                Console.WriteLine("(D)Open Door");
                isFoundDoor = true;
            }
            if (map.FindObjectNearPlayer("n", map.playerPosition))
            {
                Console.WriteLine("(T)Talk with NPC");
                isFoundNpc = true;
            }

            bool pass = true;
            do
            {
                ConsoleKey input = Console.ReadKey().Key;
                if (IsWalk(input))
                {
                    map.UpdatePlayerPosition(input);
                    map.DrawMap();
                    break;
                }
                else if (input == ConsoleKey.I)
                {
                    CutScene.scene.SelectInventory(player);
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.S)
                {
                    CutScene.scene.SelectStatus(player);
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.E)
                {
                    CutScene.scene.SelectEquipment(player);
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.F && isFoundChest)
                {
                    CutScene.scene.SelectOpenChest(map, player);
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.D && isFoundDoor)
                {
                    CutScene.scene.SelectOpenDoor();
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.T && isFoundNpc)
                {
                    int indexOfNpc = map.FindObjIdNearPlayer("n", map.playerPosition);
                    NPC npc = map.listOfNpc[indexOfNpc];
                    CutScene.scene.SelectTalkNPC(ref npc,ref player);
                    Console.Clear();
                    map.DrawMap();
                }
                else if (input == ConsoleKey.Backspace)
                {
                    pass = true;
                    Console.Clear();
                    map.DrawMap();
                }
                else
                    pass = false;
            } while (!pass);

        }
        /// <summary>
        /// check status IsWalk if player press arrow buttons.
        /// </summary>
        bool IsWalk(ConsoleKey input)
        {
            if (input == ConsoleKey.LeftArrow || input == ConsoleKey.RightArrow || input == ConsoleKey.UpArrow || input == ConsoleKey.DownArrow)
            {
                return true;
            }
            else
                return false;
        }
    }
}
