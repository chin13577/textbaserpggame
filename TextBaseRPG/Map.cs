﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    public struct Vector2
    {
        public int x;
        public int y;
        public Vector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
    class Map
    {
        public string[,] map;
        /// <summary>
        /// player location.
        /// </summary>
        public Vector2 playerPosition;
        public List<List<ItemInventory>> listOfChest = new List<List<ItemInventory>>();
        public List<NPC> listOfNpc = new List<NPC>();
        /// <summary>
        /// Update player position.
        /// </summary>
        /// <param name="key">input key</param>
        public void UpdatePlayerPosition(ConsoleKey key)
        {
            int x = playerPosition.x;
            int y = playerPosition.y;
            if (key == ConsoleKey.LeftArrow)
            {
                x = x - 1;
            }
            else if (key == ConsoleKey.RightArrow)
            {
                x = x + 1;
            }
            else if (key == ConsoleKey.UpArrow)
            {
                y = y - 1;
            }
            else if (key == ConsoleKey.DownArrow)
            {
                y = y + 1;
            }
            if (map[y, x] == "_")
            {
                map[playerPosition.y, playerPosition.x] = "_";
                playerPosition = new Vector2(x, y);
                map[playerPosition.y, playerPosition.x] = "p";
            }
        }
        /// <summary>
        /// Find id of obj near player start at player[i-1,j-1].
        /// </summary>
        /// <param name="point">Player Pos.</param>
        /// <returns>id of chest</returns>
        public int FindObjIdNearPlayer(string obj,Vector2 point)
        {
            int indexI = -1;
            int indexJ = -1;
            for (int i = 0; i < 3; i++)
            {
                indexJ = -1;
                for (int j = 0; j < 3; j++)
                {
                    if (map[point.y + indexI, point.x + indexJ].Contains(obj))
                    {
                        char[] c = map[point.y + indexI, point.x + indexJ].ToCharArray();
                        return Convert.ToInt32(c[1].ToString());
                    }
                    indexJ++;
                }
                indexI++;
            }
            return -1;
        }
        /// <summary>
        /// Find object near player radius = 1.
        /// </summary>
        /// <param name="obj">object</param>
        /// <param name="point">player's position</param>
        /// <returns>is Found.</returns>
        public bool FindObjectNearPlayer(string obj, Vector2 point)
        {
            if (map[point.y - 1, point.x - 1].Contains(obj))
                return true;
            else if (map[point.y - 1, point.x].Contains(obj))
                return true;
            else if (map[point.y - 1, point.x + 1].Contains(obj))
                return true;
            else if (map[point.y, point.x - 1].Contains(obj))
                return true;
            else if (map[point.y, point.x + 1].Contains(obj))
                return true;
            else if (map[point.y + 1, point.x - 1].Contains(obj))
                return true;
            else if (map[point.y + 1, point.x].Contains(obj))
                return true;
            else if (map[point.y + 1, point.x + 1].Contains(obj))
                return true;
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Find Position of obj near player.
        /// </summary>
        /// <param name="obj">object</param>
        /// <param name="point">player's position</param>
        /// <returns></returns>
        public Vector2 FindObjectPositionNearPlayer(string obj, Vector2 point)
        {
            if (map[point.y - 1, point.x - 1].Contains(obj))
                return new Vector2(point.x - 1, point.y - 1);
            else if (map[point.y - 1, point.x].Contains(obj))
                return new Vector2(point.x, point.y - 1);
            else if (map[point.y - 1, point.x + 1].Contains(obj))
                return new Vector2(point.x + 1, point.y - 1);
            else if (map[point.y, point.x - 1].Contains(obj))
                return new Vector2(point.x - 1, point.y);
            else if (map[point.y, point.x + 1].Contains(obj))
                return new Vector2(point.x + 1, point.y);
            else if (map[point.y + 1, point.x - 1].Contains(obj))
                return new Vector2(point.x - 1, point.y + 1);
            else if (map[point.y + 1, point.x].Contains(obj))
                return new Vector2(point.x, point.y + 1);
            else if (map[point.y + 1, point.x + 1].Contains(obj))
                return new Vector2(point.x + 1, point.y + 1);
            else
            {
                return new Vector2(-1, -1);
            }
        }
        /// <summary>
        /// read map's detail from text file.
        /// </summary>
        /// <param name="mapNum">player.currentMap</param>
        public void ReadMap(int mapNum)
        {
            TextReader tr = new StreamReader(@"./maps/map" + mapNum + ".txt");
            string[] read = tr.ReadToEnd().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int row = read.Length;

            int col = FindMaxColInMap(read);
            this.map = new string[row-1, col];

            AddNpc(read);
            AddItemToChest(read);

            for (int i = 1; i < row-1; i++)
            {
                string[] s = read[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < col; j++)
                {
                    if (j < s.Length)
                        this.map[i, j] = s[j];
                    else
                    {
                        this.map[i, j] = " ";
                        continue;
                    }
                    if (s[j] == "p")
                    {
                        playerPosition.x = j;
                        playerPosition.y = i;
                    }
                }
            }
        }
        /// <summary>
        /// Read item's list from text file.
        /// </summary>
        /// <param name="read">data</param>
        void AddItemToChest(string[] read)
        {
            // if map has many chest. use | for assign items in the chest in map_.txt.
            string[] chestCount = read[0].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < chestCount.Length; i++)
                listOfChest.Add(new List<ItemInventory>());
            // extract data from text
            for (int i = 0; i < chestCount.Length; i++)
            {
                string[] chest = chestCount[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int j = 0; j < chest.Length; j++)
                {
                    // extract item and count of item by 'count of item+ )'
                    string[] item = chest[j].Split(')');
                    listOfChest[i].Add(new ItemInventory(ItemDatabase.itemDb.GetDataById(Convert.ToInt32(item[1])), Convert.ToInt32(item[0])));
                }
            }
        }
        /// <summary>
        /// Read Npc from text file.
        /// </summary>
        /// <param name="read">data</param>
        void AddNpc(string[] read)
        {
            // if map has many chest. use | for assign items in the chest in map_.txt.
            string[] readNpc = read[read.Length-1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < readNpc.Length; i++)
            {
                string[] dataNpc = readNpc[i].Split(new char[] { ')' }, StringSplitOptions.RemoveEmptyEntries);
                listOfNpc.Add(new NPC(dataNpc[1]));
            }
        }
        public void DrawMap()
        {
            Console.Clear();
            for (int i = 0; i < 10; i++)
                Console.Write("#");
            Console.Write(" Map ");
            for (int i = 0; i < 10; i++)
                Console.Write("#");
            Console.WriteLine("\n");
            Console.WriteLine("note : p = player, c = chest, d = door, n = npc");

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == null)
                        continue;

                    if (map[i, j] == "p")
                        PrintTextWithColor(map[i, j] + "  ", ConsoleColor.Red);
                    else if (map[i, j].Contains("c"))
                        PrintTextWithColor(map[i, j] + " ", ConsoleColor.Yellow);
                    else if (map[i, j].Contains("d"))
                        PrintTextWithColor(map[i, j] + " ", ConsoleColor.White);
                    else if (map[i, j].Contains("n"))
                        PrintTextWithColor(map[i, j] + " ", ConsoleColor.Magenta);
                    else
                        Console.Write(map[i, j] + "  ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
            for (int i = 0; i < 20; i++)
                Console.Write("#");
            Console.WriteLine("\n");
        }
        int FindMaxColInMap(string[] read)
        {
            int max = 0;
            for (int i = 1; i < read.Length; i++)
            {
                int col = read[i].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Length;
                if (col > max)
                    max = col;
            }
            return max;
        }
        void PrintTextWithColor(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ResetColor();
        }
    }
}
