﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class Equipment
    {
        public enum Armors { Head, Body, L_hand, R_hand, L_leg, R_leg };
        public enum Weapons { L_hand, R_hand };
        public enum Accessorys { Neck, Belt, Cape , L_ear, R_ear,  Ring1, Ring2, Ring3 };
        /// <summary>
        /// armors = { Head, Body, L_hand, R_hand, L_leg, R_leg }
        /// </summary>
        public Item[] armors;
        /// <summary>
        /// weapons = { L_hand, R_hand }
        /// </summary>
        public Item[] weapons;
        /// <summary>
        /// accessorys = { Neck, Belt, Cape , L_ear, R_ear,  Ring1, Ring2, Ring3 }
        /// </summary>
        public Item[] accessories;
        public Equipment()
        {
            armors = new Item[6];
            weapons = new Item[2];
            accessories = new Item[8];

            for (int i = 0; i < armors.Length; i++)
            {
                armors[i] = new Item();
            }
            for (int i = 0; i < weapons.Length; i++)
            {
                weapons[i] = new Item();
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                accessories[i] = new Item();
            }
        }
        /// <summary>
        /// Check player equip weapon on input hand. true = free slot
        /// </summary>
        /// <param name="hand">weapon hand</param>
        /// <returns></returns>
        public bool CheckWeaponAvailable(Equipment.Weapons hand)
        {
            if (this.weapons[(int)hand].name != "")
                return false;
            else
                return true;
        }
        /// <summary>
        /// find index of slot
        /// </summary>
        /// <returns></returns>
        public int FindEmptySlotForWeapon()
        {
            if (weapons[(int)Equipment.Weapons.R_hand].name == "")
                return (int)Equipment.Weapons.R_hand;
            else if (weapons[(int)Equipment.Weapons.L_hand].name == "")
                return (int)Equipment.Weapons.L_hand;
            return -1;
        }
        /// <summary>
        /// find index of slots.
        /// </summary>
        /// <param name="type">armor type</param>
        /// <returns></returns>
        public int FindEmptySlotForArmor(Item.ArmorType type)
        {
            if (type == Item.ArmorType.Hand)
            {
                if (armors[(int)Armors.L_hand].name == "")
                    return (int)Armors.L_hand;
                else if (armors[(int)Armors.R_hand].name == "")
                    return (int)Armors.R_hand;
            }
            else if (type == Item.ArmorType.Leg)
            {
                if (armors[(int)Armors.L_leg].name == "")
                    return (int)Armors.L_leg;
                else if (armors[(int)Armors.R_leg].name == "")
                    return (int)Armors.R_leg;
            }
            else if (type == Item.ArmorType.Head)
            {
                if (armors[(int)Armors.Head].name == "")
                    return (int)Armors.Head;
            }
            else if (type == Item.ArmorType.Body)
            {
                if (armors[(int)Armors.Body].name == "")
                    return (int)Armors.Body;
            }
            return -1;
        }
        /// <summary>
        /// find index of slots in accessories.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int FindEmptySlotForAccessory(Item.AccessoryType type)
        {
            if (type == Item.AccessoryType.Ring)
            {
                if (accessories[(int)Accessorys.Ring1].name == "")
                    return (int)Accessorys.Ring1;
                else if (accessories[(int)Accessorys.Ring2].name == "")
                    return (int)Accessorys.Ring2;
                else if (accessories[(int)Accessorys.Ring3].name == "")
                    return (int)Accessorys.Ring3;
            }
            else if (type == Item.AccessoryType.Ear)
            {
                if (accessories[(int)Accessorys.R_ear].name == "")
                    return (int)Accessorys.R_ear;
                else if (accessories[(int)Accessorys.L_ear].name == "")
                    return (int)Accessorys.L_ear;
            }
            else
            {
                if (accessories[(int)type].name == "")
                    return (int)type;
            }
            return -1;
        }
        /// <summary>
        /// find index of item in equipment.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int FindIndexOfItemInEquipment(Item item)
        {
            if (item.itemType == Item.ItemType.Weapon)
            {
                for (int i = 0; i < weapons.Length; i++)
                {
                    if (weapons[i].serial == item.serial)
                        return i;
                }
            }
            else if (item.itemType == Item.ItemType.Armor)
            {
                for (int i = 0; i < armors.Length; i++)
                {
                    if (armors[i].serial == item.serial)
                        return i;
                }
            }
            else if (item.itemType == Item.ItemType.Accessory)
            {
                for (int i = 0; i < accessories.Length; i++)
                {
                    if (accessories[i].serial == item.serial)
                        return i;
                }
            }
            return -1;
        }
        public bool CheckArmorAvailable(Item item)
        {
            Item.ArmorType type = item.armorType;
            if (armors[(int)type].name != "")
                return false;
            else
                return true;
        }
        /// <summary>
        /// Calculate total buff maxhp from equipment.
        /// </summary>
        /// <returns></returns>
        public int TotalBuffMaxHp()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].hp;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].hp;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].hp;
            }
            return result;
        }
        /// <summary>
        /// Calculate total buff maxmp from equipment.
        /// </summary>
        /// <returns></returns>
        public int TotalBuffMaxMp()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].mp;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].mp;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].mp;
            }
            return result;
        }
        /// <summary>
        /// Calculate total buff atk from equipment.
        /// </summary>
        /// <returns></returns>
        public int TotalBuffAtk()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].atk;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].atk;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].atk;
            }
            return result;
        }
        /// <summary>
        /// Calculate total buff Def from equipment.
        /// </summary>
        /// <returns></returns>
        public int TotalBuffDef()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].def;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].def;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].def;
            }
            return result;
        }
        /// <summary>
        /// Calculate total buff multiple Def from equipment.
        /// </summary>
        /// <returns></returns>
        public float TotalBuffMulDef()
        {
            float result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].mulDef;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].mulDef;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].mulDef;
            }
            return result;
        }
        public int TotalBuffStrength()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffStrength;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffStrength;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffStrength;
            }
            return result;
        }
        public int TotalBuffAgility()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffAgility;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffAgility;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffAgility;
            }
            return result;
        }
        public int TotalBuffCharisma()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffCharisma;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffCharisma;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffCharisma;
            }
            return result;
        }
        public int TotalBuffEndurance()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffEndurance;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffEndurance;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffEndurance;
            }
            return result;
        }
        public int TotalBuffIntelligence()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffIntelligence;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffIntelligence;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffIntelligence;
            }
            return result;
        }
        public int TotalBuffLuck()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffLuck;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffLuck;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffLuck;
            }
            return result;
        }
        /// <summary>
        /// Calculate total buff multiple luck from equipment.
        /// </summary>
        /// <returns></returns>
        public float TotalBuffMulLuck()
        {
            float result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].mulLuck;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].mulLuck;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].mulLuck;
            }
            return result;
        }
        public int TotalBuffPerception()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].buffPerception;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].buffPerception;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].buffPerception;
            }
            return result;
        }
        public int TotalBuffEquipWeight()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].maxEquipWeight;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].maxEquipWeight;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].maxEquipWeight;
            }
            return result;
        }
        public int TotalBuffCarryWeight()
        {
            int result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].maxCarryWeight;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].maxCarryWeight;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].maxCarryWeight;
            }
            return result;
        }
        public float CurrentEquipWeight()
        {
            float result = 0;
            for (int i = 0; i < weapons.Length; i++)
            {
                result += weapons[i].weight;
            }
            for (int i = 0; i < armors.Length; i++)
            {
                result += armors[i].weight;
            }
            for (int i = 0; i < accessories.Length; i++)
            {
                result += accessories[i].weight;
            }
            return result;
        }
    }
}
