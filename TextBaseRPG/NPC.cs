﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class NPC : Character
    {
        public enum Race { Human, Elf };
        public enum Gender { Male, Female };
        public Race race;
        public Gender gender;
        public float carryWeight;
        public float equipWeight;
        public int maxInventory;
        public NPC(string name)
        {
            base.name = name;
            Inventory = new List<ItemInventory>();
            equipment = new Equipment();
            bonusStat = new BonusStatus();
            SetDataToNPC(name);
        }
        void SetDataToNPC(string name)
        {
            if (name == "Chinnie")
            {
                this.gold = 1000;
                Inventory.Add(new ItemInventory(ItemDatabase.itemDb.GetDataById(22), 15));
                Inventory.Add(new ItemInventory(ItemDatabase.itemDb.GetDataById(23), 15));
                Inventory.Add(new ItemInventory(ItemDatabase.itemDb.GetDataById(24), 15));
            }
            else
            {
                // Imprement other npc.
            }
        }
        /// <summary>
        /// Sell item
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public ItemInventory Sell(ref Player player, int listId, int count)
        {
            if (listId > Inventory.Count)
            {
                Console.WriteLine("item does not exist.");
                return null;
            }
            if (count > Inventory[listId].count)
                count = Inventory[listId].count;

            int pGold = player.gold;
            pGold -= Inventory[listId].item.price * count;
            if (pGold < 0)
            {
                Console.WriteLine("Not enough gold.");
                return null;
            }
            player.gold = pGold;
            this.gold += Inventory[listId].item.price * count;
            ItemInventory itemInv = new ItemInventory(Inventory[listId].item, count);
            Inventory[listId].count -= count;
            if (Inventory[listId].count == 0)
                Inventory.RemoveAt(listId);
            return itemInv;
        }
    }
}
