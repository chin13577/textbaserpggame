﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class ItemDatabase
    {
        public static ItemDatabase itemDb;
        public List<Item> itemDbList = new List<Item>();
        public ItemDatabase()
        {
            AddWeapons();
            AddArmors();
            AddAccessorys();
            AddPotions();
        }
        /// <summary>
        /// Add Weapons data to item database.
        /// </summary>
        void AddWeapons()
        {
            Item weapon;
            weapon = new Item(1, "Giant Sword of Power", Item.ItemType.Weapon, 100, 12, "");
            weapon.AddValueToWeapon(0,0, 0, 0, Item.WeaponType.Sword, Item.HandedType.TwoHanded, Item.Size.Large, 100, 0, 0, 0, 0, -50, 0);
            itemDbList.Add(weapon);
            weapon = new Item(2, "Hammer of Doom", Item.ItemType.Weapon, 100, 20, "");
            weapon.AddValueToWeapon(500,0, -250, 0, Item.WeaponType.Hammer, Item.HandedType.OneHanded, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(weapon);
            weapon = new Item(11, "Green Dragon Shield", Item.ItemType.Weapon, 100, 5, "5% chance of breaking when hit");
            weapon.AddValueToWeapon(0,500, 0, 0, Item.WeaponType.Shield, Item.HandedType.OneHanded, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(weapon);
            weapon = new Item(12, "Golden Shield", Item.ItemType.Weapon, 100, 5.5f, "Generate 10 Gold when hit");
            weapon.AddValueToWeapon(0,100, 0, 0, Item.WeaponType.Shield, Item.HandedType.OneHanded, Item.Size.Large, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(weapon);
        }
        /// <summary>
        /// Add Armors data to item database.
        /// </summary>
        void AddArmors()
        {
            Item armor;
            armor = new Item(3, "Helmet of Wisdom", Item.ItemType.Armor, 100, 2, "");
            armor.AddValueToArmor(100, Item.ArmorType.Head, Item.Size.Medium, 0, 200, 0, 0, 0, 0, 0);
            itemDbList.Add(armor);
            armor = new Item(4, "Crown of the North King", Item.ItemType.Armor, 100, 2, "+50% Defense");
            armor.AddValueToArmor(200, Item.ArmorType.Head, Item.Size.Medium, 0, 0, 200, 0, 0, 0, 0);
            itemDbList.Add(armor);
            armor = new Item(5, "Chest Plate of Luck", Item.ItemType.Armor, 100, 7, "");
            armor.AddValueToArmor(250, Item.ArmorType.Body, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 777);
            itemDbList.Add(armor);
            armor = new Item(6, "Cursed Chainmail of Gloom", Item.ItemType.Armor, 100, 9, "");
            armor.AddValueToArmor(500, Item.ArmorType.Body, Item.Size.Large, 0, 0, 0, -100, 0, 0, 0);
            itemDbList.Add(armor);
            armor = new Item(7, "Gauntlet of Fire", Item.ItemType.Armor, 100, 2, "Give Perk “Resist Fire”\n+200 Defense when equip 2");
            armor.AddValueToArmor(100, Item.ArmorType.Hand, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(armor);
            armor = new Item(8, "Gauntlet of Ice", Item.ItemType.Armor, 100, 2, "Give Perk “Resist Cold”\n+200 Defense when equip 2");
            armor.AddValueToArmor(100, Item.ArmorType.Hand, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(armor);
            armor = new Item(9, "Greave of Speed", Item.ItemType.Armor, 100, 2, "Give Perk “Windwalker” when equip 2");
            armor.AddValueToArmor(100, Item.ArmorType.Leg, Item.Size.Medium, 0, 0, 0, 0, 0, 150, 0);
            itemDbList.Add(armor);
            armor = new Item(10, "Anti-Arrow Greave", Item.ItemType.Armor, 100, 3, "30% chance to deflect arrows ");
            armor.AddValueToArmor(0, Item.ArmorType.Leg, Item.Size.Medium, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(armor);
        }
        /// <summary>
        /// Add Accessorys to item database.
        /// </summary>
        void AddAccessorys()
        {
            Item acc;
            acc = new Item(13, "Wizardry Ring", Item.ItemType.Accessory, 100, 0.1f, "");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Ring, Item.Size.Small, 0, 0, 0, 0, 100, 0, 0);
            itemDbList.Add(acc);
            acc = new Item(14, "Ring of Ultima", Item.ItemType.Accessory, 100, 0.1f, "");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Neck, Item.Size.Small, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(acc);
            acc = new Item(15, "Ring of Elemental", Item.ItemType.Accessory, 100, 0.1f, "Turn the Perk “Resist Fire” into “Absorb Fire”\nTurn the Perk “Resist Cold” into “Absorb Cold”");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Neck, Item.Size.Small, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(acc);
            acc = new Item(16, "Amulet of the Beholder", Item.ItemType.Accessory, 100, 0.2f, "");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Neck, Item.Size.Small, 0, 200, 0, 0, 0, 0, 0);
            itemDbList.Add(acc);
            acc = new Item(17, "Diamond Amulet", Item.ItemType.Accessory, 1000, 0.2f, "");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Neck, Item.Size.Small, 0, 0, 0, 0, 0, 0, 0);
            itemDbList.Add(acc);
            acc = new Item(18, "Red Vampire Cape", Item.ItemType.Accessory, 100, 0.5f, "+400 Charisma when talking to character of opposite sex");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Cape, Item.Size.Small, 0, 0, 0, 0, 0, 0, 100);
            itemDbList.Add(acc);
            acc = new Item(19, "Cape of the Wind", Item.ItemType.Accessory, 100, 0.5f, "+300 Agility with the “Windwalker” Perk");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Cape, Item.Size.Small, 0, 0, 0, 0, 0, 200, 0);
            itemDbList.Add(acc);
            acc = new Item(20, "Magical Bag of Holding", Item.ItemType.Accessory, 100, 0, "Can hold 9 large items or 20 medium items");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Cape, Item.Size.Small, 0, 0, 0, 0, 0, 200, 0, 99);
            itemDbList.Add(acc);
            acc = new Item(21, "Pocketed Leather Belt", Item.ItemType.Accessory, 100, 0.5f, "Allow up to 10 potions or consumables to be used during combat");
            acc.AddValueToAccessory(0, 0, Item.AccessoryType.Belt, Item.Size.Small, 0, 0, 0, 0, 0, 200, 0, 0, 10);
            itemDbList.Add(acc);
        }
        /// <summary>
        /// Add Potions to item database.
        /// </summary>
        void AddPotions()
        {
            Item potion;
            potion = new Item(22, "Health Potion", Item.ItemType.Potion, 100, 0.5f, "");
            potion.AddValueToPotion(10,0,Item.PotionType.Normal,Item.Size.Small);
            itemDbList.Add(potion);
            potion = new Item(23, "Mana Potion", Item.ItemType.Potion, 100, 0.5f, "");
            potion.AddValueToPotion(10, 0, Item.PotionType.Normal, Item.Size.Small);
            itemDbList.Add(potion);
            potion = new Item(24, "Antidote ", Item.ItemType.Potion, 100, 0.5f, "");
            potion.AddValueToPotion(10, 0, Item.PotionType.Antidote, Item.Size.Small);
            itemDbList.Add(potion);
        }
        public Item GetDataByName(string name)
        {
            for (int i = 0; i < itemDbList.Count; i++)
            {
                if (itemDbList[i].name == name)
                    return itemDbList[i].GetCopy();
            }
            return new Item();
        }
        public Item GetDataById(int id)
        {
            for (int i = 0; i < itemDbList.Count; i++)
            {
                if (itemDbList[i].id == id)
                    return itemDbList[i].GetCopy();
            }
            return new Item();
        }
    }
}
