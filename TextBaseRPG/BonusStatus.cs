﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class BonusStatus
    {
        //Main Attributes
        public int strength;
        public int perception;
        public int endurance;
        public int charisma;
        public int intelligence;
        public int agility;
        public int luck;

        //Combat
        public int atk;
        public int def;
        public int hp;
        public int mp;

        public int maxCarryWeight;
        public int maxEquipWeight;
        //Special Attributes
        public int deflectArrows;
        /// <summary>
        /// perks of player.
        /// </summary>
        public List<string> perks = new List<string>();
        /// <summary>
        /// get bonus status.
        /// </summary>
        /// <param name="equip"></param>
        /// <returns></returns>
        public BonusStatus GetBonusStatus(Equipment equip)
        {
            return EquipmentCombo.equipCombo.GetBonusStatus(equip);
        }
        /// <summary>
        /// check Perk of player exist.
        /// </summary>
        /// <param name="name">Perk's name</param>
        /// <returns></returns>
        public bool CheckPerkExist(string name)
        {
            for (int i = 0; i < perks.Count; i++)
            {
                if (name == perks[i])
                    return true;
            }
            return false;
        }
        /// <summary>
        /// return index of Perk in list of Perk.
        /// </summary>
        /// <param name="name">Perk name</param>
        /// <returns>index of Perk</returns>
        public int FindPerkIndex(string name)
        {
            for (int i = 0; i < perks.Count; i++)
            {
                if (name == perks[i])
                    return i;
            }
            return -1;
        }
    }
}
