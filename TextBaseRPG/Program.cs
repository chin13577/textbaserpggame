﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class Program
    {
        public static Player player;
        public static UserController controller;
        static void Main(string[] args)
        {
            SetUpEnvironment();
            CutScene.scene.StartScene(player);
            Console.Clear();
            controller.player = Program.player;
            controller.map.ReadMap(player.currentMap);
            controller.map.DrawMap();
            while (true)
            {
                controller.GetCommandInput();
            }
        }
        static void SetUpEnvironment()
        {
            ItemDatabase.itemDb = new ItemDatabase();
            controller = new UserController();
            EquipmentCombo.equipCombo = new EquipmentCombo();
            CutScene.scene = new CutScene();
        }
    }
}
