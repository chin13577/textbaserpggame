﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class Character
    {
        public string name;
        //Main Attributes
        protected int baseStrength;
        protected int basePerception;
        protected int baseEndurance;
        protected int baseCharisma;
        protected int baseIntelligence;
        protected int baseAgility;
        protected int baseLuck;

        //Combat
        public int atk;
        public int def;
        protected int baseHp;
        protected int baseMp;
        //Others
        public float maxCarryWeight;
        public float maxEquipWeight;
        public int gold;

        public List<ItemInventory> Inventory;
        public Equipment equipment;
        public BonusStatus bonusStat;
    }
}
