﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    /// <summary>
    /// services about calculate Bonus status from player's equipment.
    /// </summary>
    class EquipmentCombo
    {
        public static EquipmentCombo equipCombo;
        List<string> bonusSet;
        /// <summary>
        /// Calculate all bonus status from equipment.
        /// </summary>
        /// <param name="equip">equipment of player.</param>
        /// <returns></returns>
        public BonusStatus GetBonusStatus(Equipment equip)
        {
            bonusSet = new List<string>();
            BonusStatus bStat = new BonusStatus();
            bStat = CheckBonusStatusInWeapon(bStat, equip);
            bStat = CheckBonusStatusInArmor(bStat, equip);
            bStat = CheckBonusStatusInAccessory(bStat, equip);
            return bStat;
        }
        /// <summary>
        /// Calculate bonus status from weapons.
        /// </summary>
        /// <param name="bStat">player's bonusStat</param>
        /// <param name="equip"></param>
        /// <returns>BonusStatus</returns>
        BonusStatus CheckBonusStatusInWeapon(BonusStatus bStat, Equipment equip)
        {
            for (int i = 0; i < equip.weapons.Length; i++)
            {
                string name = equip.weapons[i].name;
                switch (name)
                {
                    case "Green Dragon Shield":
                        if (bStat.CheckPerkExist("5% chance of breaking when hit") == false)
                            bStat.perks.Add("5% chance of breaking when hit");
                        equip.weapons[i].chanceBreaking = 5;
                        break;
                    case "Golden Shield":
                        if (bStat.CheckPerkExist("Generate 10 Gold when hit") == false)
                            bStat.perks.Add("Generate 10 Gold when hit");
                        break;
                }
            }
            return bStat;
        }
        /// <summary>
        /// Calculate bonus status from armors.
        /// </summary>
        /// <param name="bStat">player's bonusStat.</param>
        /// <param name="equip">player's equipment.</param>
        /// <returns>BonusStatus</returns>
        BonusStatus CheckBonusStatusInArmor(BonusStatus bStat, Equipment equip)
        {
            for (int i = 0; i < equip.armors.Length; i++)
            {
                string name = equip.armors[i].name;
                switch (name)
                {
                    case "Crown of the North King":
                        equip.armors[i].mulDef += +0.5f;
                        break;
                    case "Cursed Chainmail of Gloom":
                        if (bStat.CheckPerkExist("Cannot equipped shields") == false)
                            bStat.perks.Add("Cannot equipped shields");
                        equip.armors[i].mulLuck += -0.3f;
                        break;
                    case "Gauntlet of Fire":
                        if (bStat.CheckPerkExist("Resist Fire") == false)
                            bStat.perks.Add("Resist Fire");
                        if (CheckBonusSetExist(name) == false && equip.armors[(int)Equipment.Armors.L_hand].name == name && equip.armors[(int)Equipment.Armors.R_hand].name == name)
                        {
                            bStat.def += 200;
                            bonusSet.Add(name);
                        }
                        break;
                    case "Gauntlet of Ice":
                        if (bStat.CheckPerkExist("Resist Cold") == false)
                            bStat.perks.Add("Resist Cold");
                        if (CheckBonusSetExist(name) == false && equip.armors[(int)Equipment.Armors.L_hand].name == name && equip.armors[(int)Equipment.Armors.R_hand].name == name)
                        {
                            bStat.def += 200;
                            bonusSet.Add(name);
                        }
                        break;
                    case "Greave of Speed":
                        if (CheckBonusSetExist(name) == false && equip.armors[(int)Equipment.Armors.L_leg].name == name && equip.armors[(int)Equipment.Armors.R_leg].name == name)
                        {
                            bonusSet.Add(name);
                            if (bStat.CheckPerkExist("Windwalker") == false)
                                bStat.perks.Add("Windwalker");
                        }
                        break;
                    case "Anti-Arrow Greave":
                        if (bStat.CheckPerkExist("30% chance to deflect arrows") == false)
                            bStat.perks.Add("30% chance to deflect arrows");
                        if (CheckBonusSetExist(name) == false && equip.armors[(int)Equipment.Armors.L_leg].name == name && equip.armors[(int)Equipment.Armors.R_leg].name == name)
                        {
                            bonusSet.Add(name);
                            bStat.deflectArrows += 30;
                        }
                        break;
                }
            }
            return bStat;
        }
        /// <summary>
        /// Calculate bonus status from accessories.
        /// </summary>
        /// <param name="bStat">player's bonusStat.</param>
        /// <param name="equip">player's equipment.</param>
        /// <returns>BonusStatus</returns>
        BonusStatus CheckBonusStatusInAccessory(BonusStatus bStat, Equipment equip)
        {
            for (int i = 0; i < equip.accessories.Length; i++)
            {
                string name = equip.accessories[i].name;
                switch (name)
                {
                    case "Wizardry Ring":
                        if (CheckBonusSetExist(name) == false)
                        {
                            bonusSet.Add(name);
                            int count = 0;
                            for (int j = 5; j < 8; j++)
                            {
                                if (equip.accessories[j].name == name)
                                    count++;
                            }
                            if (count >= 2)
                                bStat.mp += 500;
                            if (count == 3)
                            {
                                if (bStat.CheckPerkExist("Wizard King") == false)
                                    bStat.perks.Add("Wizard King");
                            }
                        }
                        break;
                    case "Ring of Ultima":
                        if (CheckBonusSetExist(name) == false)
                        {
                            bonusSet.Add(name);
                            int count = 0;
                            for (int j = 5; j < 8; j++)
                            {
                                if (equip.accessories[j].name == name)
                                    count++;
                            }
                            if (count >= 2)
                                bStat.hp += 500;
                            if (count == 3)
                            {
                                bStat.mp += 500;
                                if (count == 3)
                                {
                                    if (bStat.CheckPerkExist("Wizard King") == false)
                                        bStat.perks.Add("Wizard King");
                                }
                            }
                        }
                        break;
                    case "Ring of Elemental":
                        int index = bStat.FindPerkIndex("Resist Fire");
                        if (index != -1)
                            bStat.perks[index] = "Absorb Fire";
                        index = bStat.FindPerkIndex("Resist Cold");
                        if (index != -1)
                            bStat.perks[index] = "Absorb Cold";
                        break;
                    case "Red Vampire Cape":
                        if (bStat.CheckPerkExist("Red Vampire Cape") == false)
                            bStat.perks.Add("Red Vampire Cape");
                        break;
                    case "Cape of the Wind":
                        if (bStat.CheckPerkExist("Windwalker") == true)
                            bStat.agility += 300;
                        break;
                    case "Pocketed Leather Belt":
                        if (bStat.CheckPerkExist("Allow up to 10 potions or consumables to be used during combat") == false)
                            bStat.perks.Add("Allow up to 10 potions or consumables to be used during combat");
                        bStat.maxEquipWeight += 10;
                        break;


                }
            }
            return bStat;
        }
        /// <summary>
        /// check item set are gotten the value.
        /// </summary>
        /// <param name="name">item name</param>
        /// <returns></returns>
        bool CheckBonusSetExist(string name)
        {
            for (int i = 0; i < bonusSet.Count; i++)
            {
                if (name == bonusSet[i])
                    return true;
            }
            return false;
        }
    }
}
