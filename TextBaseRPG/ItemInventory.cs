﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class ItemInventory
    {
        public Item item;
        public int count;
        public ItemInventory(Item i, int count)
        {
            this.item = i;
            this.count = count;
        }
    }
}
