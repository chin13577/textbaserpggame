﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class Item
    {
        public enum ItemType { Material, Weapon, Armor, Accessory, Potion };
        public enum WeaponType { None, Sword, Hammer, Axe, Bow, Staff, Dagger, Shield };
        public enum HandedType { None, OneHanded, TwoHanded };
        public enum ArmorType { Head, Body, Hand, Leg };
        public enum AccessoryType { Neck, Belt, Cape, Ear, Ring };
        public enum PotionType { Normal, Antidote };
        public enum Size { Small, Medium, Large };
        public ItemType itemType;
        public HandedType handedType;
        public WeaponType weaponType;
        public ArmorType armorType;
        public AccessoryType accessoryType;
        public PotionType potionType;
        public Size size;
        public int id;
        public float serial;
        public string name;
        public int price;
        public float weight;
        public string detail;
        //Buffs
        public int atk;
        public int def;
        public int hp;
        public int mp;
        public int buffStrength;
        public int buffPerception;
        public int buffEndurance;
        public int buffCharisma;
        public int buffIntelligence;
        public int buffAgility;
        public int buffLuck;
        public int maxCarryWeight;
        public int maxEquipWeight;

        //Special Attribute
        public int chanceBreaking;
        public float mulDef ;
        public float mulLuck ;

        public Item()
        {
            this.name = "";
            this.detail = "";
        }
        public Item(int id, string name, ItemType type, int price, float weight, string detail)
        {
            this.id = id;
            this.name = name;
            this.itemType = type;
            this.price = price;
            this.weight = weight;
            this.detail = detail;
        }
        public void AddValueToWeapon(int atk,int def, int hp, int mp, WeaponType weaponType, HandedType handedType, Size size, int strength, int perception, int endurance, int charisma, int intelligence,
                                     int agility, int luck)
        {
            this.atk = atk;
            this.def = def;
            this.hp = hp;
            this.mp = mp;
            this.weaponType = weaponType;
            this.handedType = handedType;
            this.size = size;
            this.buffStrength = strength;
            this.buffPerception = perception;
            this.buffEndurance = endurance;
            this.buffCharisma = charisma;
            this.buffIntelligence = intelligence;
            this.buffAgility = agility;
            this.buffLuck = luck;
        }
        public void AddValueToArmor(int def, ArmorType armorType, Size size, int strength, int perception, int endurance, int charisma, int intelligence,
                                     int agility, int luck)
        {
            this.def = def;
            this.armorType = armorType;
            this.size = size;
            this.buffStrength = strength;
            this.buffPerception = perception;
            this.buffEndurance = endurance;
            this.buffCharisma = charisma;
            this.buffIntelligence = intelligence;
            this.buffAgility = agility;
            this.buffLuck = luck;
        }
        public void AddValueToAccessory(int def, int mp, AccessoryType accType, Size size, int strength, int perception, int endurance, int charisma, int intelligence,
                                    int agility, int luck, int maxCarryWeight = 0, int maxEquipWeight = 0)
        {
            this.def = def;
            this.mp = mp;
            this.accessoryType = accType;
            this.size = size;
            this.buffStrength = strength;
            this.buffPerception = perception;
            this.buffEndurance = endurance;
            this.buffCharisma = charisma;
            this.buffIntelligence = intelligence;
            this.buffAgility = agility;
            this.buffLuck = luck;
            this.maxCarryWeight = maxCarryWeight;
            this.maxEquipWeight = maxEquipWeight;
        }
        public void AddValueToPotion(int hp, int mp, PotionType potionType, Size size)
        {
            this.potionType = potionType;
            this.size = size;
            this.hp = hp;
            this.mp = mp;
        }
        public string CheckAvaliableIntData(string data, int value)
        {
            if (value == 0)
                return "";
            else
                return data + " : " + value + "\n";
        }
        public string CheckAvaliableFloatData(string data, float value)
        {
            if (value == 0)
                return "";
            else
                return data + " : " + value + "\n";
        }
        public string CheckAvaliableStrData(string data, string value)
        {
            if (value == "")
                return "";
            else
                return data + " : " + value + "\n";
        }
        public Item GetCopy()
        {
            return (Item)this.MemberwiseClone();
        }
    }
}
