﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class Player : Character
    {
        public int currentMap;
        //Main Attributes

        public int buffStrength;
        public int buffPerception;
        public int buffEndurance;
        public int buffCharisma;
        public int buffIntelligence;
        public int buffAgility;
        public int buffLuck;
        //Combat
        public int maxHp;
        public int curHp;
        public int maxMp;
        public int curMp;
        //Others
        public enum Race { Human, Elf };
        public enum Gender { Male, Female };
        public Race race;
        public Gender gender;
        public float carryWeight;
        public float equipWeight;
        public int maxInventory;
        
        public Player(string name, Race race, Gender gender)
        {
            this.name = name;
            if (race == Race.Human)
            {
                baseHp = 250;
                baseMp = 150;
                baseStrength = 50;
                basePerception = 20;
                baseEndurance = 30;
                baseCharisma = 50;
                baseIntelligence = 25;
                baseAgility = 25;
                baseLuck = 30;
            }
            else if (race == Race.Elf)
            {
                baseHp = 200;
                baseMp = 200;
                baseStrength = 35;
                basePerception = 35;
                baseEndurance = 30;
                baseCharisma = 50;
                baseIntelligence = 35;
                baseAgility = 35;
                baseLuck = 30;
            }
            this.race = race;
            this.gender = gender;
            this.gold = 2000;
            this.maxInventory = 25;
            Inventory = new List<ItemInventory>();
            equipment = new Equipment();
            bonusStat = new BonusStatus();
            InitialCombatValue();
            curHp = maxHp / 2;
            curMp = maxMp / 2;
        }
        /// <summary>
        /// Initial base status of player.
        /// </summary>
        void InitialCombatValue()
        {
            atk = System.Convert.ToInt32(GetTotalStrength() * 1.4 + GetTotalAgility() * 1.2);
            def = System.Convert.ToInt32(GetTotalEndurance() * 1.2);
            maxHp = baseHp + System.Convert.ToInt32(GetTotalStrength() * 0.5 + GetTotalEndurance() * 1.3);
            maxMp = baseMp + System.Convert.ToInt32(GetTotalIntelligence() * 1.2 + GetTotalPerception() * 1.2);
            curHp = maxHp;
            curMp = maxMp;
            maxCarryWeight = System.Convert.ToInt32(GetTotalStrength() * 1 + GetTotalEndurance() * 1.3 + GetTotalCharisma() * 2);
            maxEquipWeight = System.Convert.ToInt32(GetTotalStrength() * 1 + GetTotalEndurance() * 1.3 + GetTotalCharisma() * 2);
        }
        /// <summary>
        /// Update player's status.
        /// </summary>
        public void UpdateStatus()
        {
            bonusStat = bonusStat.GetBonusStatus(base.equipment);
            buffStrength = equipment.TotalBuffStrength() + bonusStat.strength;
            buffPerception = equipment.TotalBuffPerception() + bonusStat.perception;
            buffEndurance = equipment.TotalBuffEndurance() + bonusStat.endurance;
            buffCharisma = equipment.TotalBuffCharisma() + bonusStat.charisma;
            buffIntelligence = equipment.TotalBuffIntelligence() + bonusStat.intelligence;
            buffAgility = equipment.TotalBuffAgility() + bonusStat.agility;
            buffLuck = equipment.TotalBuffLuck() + bonusStat.luck + Convert.ToInt32(baseLuck * equipment.TotalBuffMulLuck());
            buffLuck = buffLuck + Convert.ToInt32(buffLuck * equipment.TotalBuffMulLuck());
            equipWeight = equipment.CurrentEquipWeight();
            carryWeight = CurrentCarryWeight();

            //UpdateCombatValue
            float hpRatio = (float)curHp / (float)maxHp;
            float mpRatio = (float)curMp / (float)maxMp;
            //Calculate new value.
            maxHp = baseHp + System.Convert.ToInt32(GetTotalStrength() * 0.5 + GetTotalEndurance() * 1.3) + equipment.TotalBuffMaxHp() + bonusStat.hp;
            maxMp = baseMp + System.Convert.ToInt32(GetTotalIntelligence() * 1.2 + GetTotalPerception() * 1.2) + equipment.TotalBuffMaxMp() + bonusStat.mp;
            atk = System.Convert.ToInt32(GetTotalStrength() * 1.4 + GetTotalAgility() * 1.2) + equipment.TotalBuffAtk() + bonusStat.atk;
            def = System.Convert.ToInt32(GetTotalEndurance() * 1.2 + equipment.TotalBuffDef() + bonusStat.def);
            def = def + Convert.ToInt32(def * equipment.TotalBuffMulDef());
            maxCarryWeight = System.Convert.ToInt32(baseStrength * 2 + baseEndurance * 2 + baseCharisma * 2) + bonusStat.maxCarryWeight;
            maxEquipWeight = System.Convert.ToInt32(baseStrength * 2 + baseEndurance * 2 + baseCharisma * 2) + bonusStat.maxEquipWeight;

            curHp = System.Convert.ToInt32(hpRatio * maxHp);
            curMp = System.Convert.ToInt32(mpRatio * maxMp);

        }
        /// <summary>
        /// Add item in inventory
        /// </summary>
        /// <param name="item"></param>
        /// <param name="count"></param>
        public void AddItem(Item item, int count)
        {
            if (item.itemType == Item.ItemType.Material || item.itemType == Item.ItemType.Potion)
            {
                int indexOfItem = FindIndexOfItemBySerial(item.serial);
                if (indexOfItem != -1)
                {
                    Inventory[indexOfItem].count += count;
                    return;
                }
            }
            if (Inventory.Count < this.maxInventory)
            {
                //random serial number for the easy way to find item and reference to this item.
                item.serial = Convert.ToSingle(new Random().NextDouble());
                Inventory.Add(new ItemInventory(item, count));
            }
            else
                Console.WriteLine("Inventory is full");
            UpdateStatus();
        }
        /// <summary>
        /// Delete Item in player's inventory.
        /// </summary>
        /// <param name="item"></param>
        public void DeleteItemInInventory(Item item)
        {
            int indexOfItem = FindIndexOfItemBySerial(item.serial);
            if (indexOfItem != -1)
            {
                if (--Inventory[indexOfItem].count == 0)
                    Inventory.RemoveAt(indexOfItem);
            }
        }
        /// <summary>
        /// Find index of item using item.serial
        /// </summary>
        /// <param name="serial">item.serial</param>
        /// <returns>Index of Item in List of Inventory</returns>
        int FindIndexOfItemBySerial(float serial)
        {
            for (int i = 0; i < Inventory.Count; i++)
            {
                if (Inventory[i].item.serial == serial)
                    return i;
            }
            return -1;
        }
        #region Equip item
        /// <summary>
        /// Equip item
        /// </summary>
        /// <param name="item">item from player's inventory</param>
        /// <returns></returns>
        public bool EquipItem(Item item)
        {
            if (item.itemType == Item.ItemType.Weapon)
            {
                if (EquipWeapon(item) == false)
                    return false;
            }
            else if (item.itemType == Item.ItemType.Armor)
            {
                EquipArmor(item);
            }
            else if (item.itemType == Item.ItemType.Accessory)
            {
                EquipAccessory(item);
            }
            else if (item.itemType == Item.ItemType.Potion)
            {
                UseItem(item);
            }
            UpdateStatus();
            return true;
        }
        /// <summary>
        /// un equip item from player's equipment.
        /// </summary>
        /// <param name="equipItem">item in equipment that player equip.</param>
        /// <returns></returns>
        public bool UnEquipItem(Item equipItem)
        {
            if (Inventory.Count > maxInventory)
            {
                Console.WriteLine("Your Inventory is full");
                return false;
            }
            int index = equipment.FindIndexOfItemInEquipment(equipItem);
            if (index != -1)
            {
                if (equipItem.itemType == Item.ItemType.Weapon)
                {
                    Item temp = equipment.weapons[index].GetCopy();
                    equipment.weapons[index] = new Item();
                    AddItem(temp, 1);
                }
                else if (equipItem.itemType == Item.ItemType.Armor)
                {
                    Item temp = equipment.armors[index].GetCopy();
                    equipment.armors[index] = new Item();
                    AddItem(temp, 1);
                }
                else if (equipItem.itemType == Item.ItemType.Accessory)
                {
                    Item temp = equipment.accessories[index].GetCopy();
                    equipment.accessories[index] = new Item();
                    AddItem(temp, 1);
                }
            }
            else
                return false;
            UpdateStatus();
            return true;
        }
        /// <summary>
        /// Equip Armor
        /// </summary>
        /// <param name="item">item in inventory</param>
        /// <returns></returns>
        bool EquipArmor(Item item)
        {
            int indexAvailable = equipment.FindEmptySlotForArmor(item.armorType);
            // Available
            if (indexAvailable != -1)
            {
                equipment.armors[indexAvailable] = item.GetCopy();
                DeleteItemInInventory(item);
            }
            else
            {
                Item temp = new Item();
                if (item.armorType == Item.ArmorType.Hand)
                {
                    temp = equipment.armors[(int)Equipment.Armors.R_hand].GetCopy();
                    equipment.armors[(int)Equipment.Armors.R_hand] = item.GetCopy();
                }
                else if (item.armorType == Item.ArmorType.Leg)
                {
                    temp = equipment.armors[(int)Equipment.Armors.R_leg].GetCopy();
                    equipment.armors[(int)Equipment.Armors.R_leg] = item.GetCopy();
                }
                else
                {
                    temp = equipment.armors[(int)item.armorType].GetCopy();
                    equipment.armors[(int)item.armorType] = item.GetCopy();
                }
                DeleteItemInInventory(item);
                AddItem(temp, 1);
            }
            return true;
        }
        /// <summary>
        /// Equip Accessory
        /// </summary>
        /// <param name="item">item in inventory</param>
        /// <returns></returns>
        bool EquipAccessory(Item item)
        {
            int indexAvailable = equipment.FindEmptySlotForAccessory(item.accessoryType);
            // Available
            if (indexAvailable != -1)
            {
                equipment.accessories[indexAvailable] = item.GetCopy();
                DeleteItemInInventory(item);
            }
            else
            {
                Item temp = new Item();
                if (item.accessoryType == Item.AccessoryType.Ear)
                {
                    temp = equipment.accessories[(int)Equipment.Accessorys.R_ear].GetCopy();
                    equipment.accessories[(int)Equipment.Accessorys.R_ear] = item.GetCopy();
                }
                else if (item.accessoryType == Item.AccessoryType.Ring)
                {
                    temp = equipment.accessories[(int)Equipment.Accessorys.Ring1].GetCopy();
                    equipment.accessories[(int)Equipment.Accessorys.Ring1] = item.GetCopy();
                }
                else
                {
                    temp = equipment.accessories[(int)item.accessoryType].GetCopy();
                    equipment.accessories[(int)item.accessoryType] = item.GetCopy();
                }
                DeleteItemInInventory(item);
                AddItem(temp, 1);
            }
            return true;
        }
        /// <summary>
        /// Equip weapon. player can equip one handed x2 (dual weapon).1st Priority is R_Hand. and you can equip two handed weapon when L_hand is empty and Shield equip in L_Hand Only.
        /// </summary>
        /// <param name="item">item in inventory</param>
        /// <returns></returns>
        bool EquipWeapon(Item item)
        {
            if (item.handedType == Item.HandedType.OneHanded)
            {
                // check if equip 2 handed first because player can equip 2 one handed weapon.
                if (equipment.weapons[(int)Equipment.Weapons.R_hand].handedType == Item.HandedType.TwoHanded)
                {
                    if(item.weaponType== Item.WeaponType.Shield)
                    {
                        Console.WriteLine("Cannot equip because you are equipping Two-Handed weapon");
                        return false;
                    }
                    else
                    {
                        Item temp = equipment.weapons[(int)Equipment.Weapons.R_hand].GetCopy();
                        equipment.weapons[(int)Equipment.Weapons.R_hand] = item.GetCopy();
                        if (GetTotalMaxHp() > 1)
                        {
                            DeleteItemInInventory(item);
                            AddItem(temp, 1);
                            return true;
                        }
                        else
                        {
                            equipment.weapons[(int)Equipment.Weapons.R_hand] = temp;
                            Console.WriteLine("Not enough max hp for equip '{0}'", item.name);
                            return false;
                        }
                    }
                }
                if(item.weaponType == Item.WeaponType.Shield)
                {
                    //Available
                    if (equipment.weapons[(int)Equipment.Weapons.L_hand].name == "")
                    {
                        equipment.weapons[(int)Equipment.Weapons.L_hand] = item.GetCopy();
                        DeleteItemInInventory(item);
                        return true;
                    }
                    else
                    {
                        Item temp = equipment.weapons[(int)Equipment.Weapons.L_hand].GetCopy();
                        equipment.weapons[(int)Equipment.Weapons.L_hand] = item.GetCopy();
                        DeleteItemInInventory(item);
                        AddItem(temp,1);
                        return true;
                    }
                }
                int indexAvailable = equipment.FindEmptySlotForWeapon();
                // Available
                if (indexAvailable != -1)
                {
                    Item temp = equipment.weapons[indexAvailable].GetCopy();
                    equipment.weapons[indexAvailable] = item.GetCopy();
                    if (GetTotalMaxHp() > 1)
                    {
                        DeleteItemInInventory(item);
                    }
                    else
                    {
                        equipment.weapons[indexAvailable] = temp;
                        Console.WriteLine("Not enough max hp for equip '{0}'", item.name);
                        return false;
                    }
                }
                else
                {
                    Item temp = equipment.weapons[(int)Equipment.Weapons.R_hand].GetCopy();
                    equipment.weapons[(int)Equipment.Weapons.R_hand] = item.GetCopy();
                    if (GetTotalMaxHp() > 1)
                    {
                        DeleteItemInInventory(item);
                        AddItem(temp, 1);
                    }
                    else
                    {
                        equipment.weapons[indexAvailable] = temp;
                        Console.WriteLine("Not enough max hp for equip '{0}'", item.name);
                        return false;
                    }
                }
            }
            else if (item.handedType == Item.HandedType.TwoHanded)
            {
                if (equipment.CheckWeaponAvailable(Equipment.Weapons.L_hand))
                {
                    if (equipment.CheckWeaponAvailable(Equipment.Weapons.R_hand))
                    {
                        equipment.weapons[(int)Equipment.Weapons.R_hand] = item.GetCopy();
                        DeleteItemInInventory(item);
                    }
                    else
                    {
                        Item temp = equipment.weapons[(int)Equipment.Weapons.R_hand].GetCopy();
                        equipment.weapons[(int)Equipment.Weapons.R_hand] = item.GetCopy();
                        DeleteItemInInventory(item);
                        AddItem(temp, 1);
                    }
                }
                else
                {
                    Console.WriteLine("Slot of Weapons : L_hand is not available.");
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// use item.
        /// </summary>
        /// <param name="item">item in player's inventory.</param>
        /// <returns></returns>
        bool UseItem(Item item)
        {
            if (item.potionType == Item.PotionType.Normal)
            {
                curHp += item.hp;
                curMp += item.mp;
                if (curHp > maxHp)
                    curHp = maxHp;
                if (curMp > maxMp)
                    curMp = maxMp;
            }
            else if (item.potionType == Item.PotionType.Antidote)
            {
                // imprement using Antidote.
            }
            DeleteItemInInventory(item);
            return true;
        }
        #endregion
        #region Get status player
        public int GetTotalMaxHp()
        {
            return baseHp + System.Convert.ToInt32(GetTotalStrength() * 0.5 + GetTotalEndurance() * 1.3) + equipment.TotalBuffMaxHp() + bonusStat.hp;
        }
        public int GetTotalMaxMp()
        {
            return baseMp + System.Convert.ToInt32(GetTotalIntelligence() * 1.2 + GetTotalPerception() * 1.2) + equipment.TotalBuffMaxMp() + bonusStat.mp;
        }
        public int GetTotalStrength()
        {
            if ((baseStrength + buffStrength) > 0)
                return baseStrength + buffStrength;
            else return 0;
        }
        public int GetTotalAgility()
        {
            if ((baseAgility + buffAgility) > 0)
                return baseAgility + buffAgility;
            else
                return 0;
        }
        public int GetTotalEndurance()
        {
            if ((baseEndurance + buffEndurance) > 0)
                return baseEndurance + buffEndurance;
            else
                return 0;
        }
        public int GetTotalPerception()
        {
            if ((basePerception + buffPerception) > 0)
                return basePerception + buffPerception;
            else
                return 0;
        }
        public int GetTotalIntelligence()
        {
            if ((baseIntelligence + buffIntelligence) > 0)
                return baseIntelligence + buffIntelligence;
            else
                return 0;
        }
        public int GetTotalLuck()
        {
            if ((baseLuck + buffLuck) > 0)
                return baseLuck + buffLuck;
            else
                return 0;
        }
        public int GetTotalCharisma()
        {
            if ((baseCharisma + buffCharisma) > 0)
                return baseCharisma + buffCharisma;
            else
                return 0;
        }
        public float CurrentCarryWeight()
        {
            float result = 0;
            for (int i = 0; i < Inventory.Count; i++)
            {
                result += Inventory[i].item.weight * Inventory[i].count;
            }
            return result;
        }
        #endregion
    }
}
