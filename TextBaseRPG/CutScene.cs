﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextBaseRPG
{
    class CutScene
    {
        public static CutScene scene;
        public void StartScene(Player player)
        {
            Console.WriteLine("Welcome to TextBase RPG World. Please create your character \ninput: 'Y' = ok, 'N' = exit.");
            BEGIN:
            Console.Write("# ");
            string input = Console.ReadLine().ToUpper();
            if (input == "Y")
            {
                Program.player = CreateCharacter(player);
            }
            else if (input == "N")
            {
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("error please try again.");
                goto BEGIN;
            }

        }
        /// <summary>
        /// create flow when select inventory.
        /// </summary>
        public void SelectInventory(Player player)
        {
            while (true)
            {
                DrawScreen.DrawInventoryScreen(player);
                ConsoleKey read = Console.ReadKey().Key;
                if (read == ConsoleKey.D)
                {
                    if (player.Inventory.Count == 0)
                        continue;
                    INSERTNUMBER:
                    int num;
                    Console.Write("Insert number : ");
                    string str = Console.ReadLine();
                    if (Int32.TryParse(str, out num) == false)
                        goto INSERTNUMBER;
                    if (num > player.Inventory.Count || num <= 0)
                    {
                        Console.WriteLine("{0} is out of length", num);
                        goto INSERTNUMBER;
                    }
                    // print detail.
                    int indexOfItem = num - 1;
                    do
                    {
                        DrawScreen.DrawDetailOfItemScreen(player.Inventory[indexOfItem].item);
                    }
                    while (Console.ReadKey().Key != ConsoleKey.Backspace);
                }
                else if (read == ConsoleKey.R)
                {
                    if (player.Inventory.Count == 0)
                        continue;
                    INSERTNUMBER:
                    int num;
                    Console.Write("Insert number : ");
                    string str = Console.ReadLine();
                    if (Int32.TryParse(str, out num) == false)
                        goto INSERTNUMBER;
                    if (num > player.Inventory.Count || num <= 0)
                    {
                        Console.WriteLine("{0} is out of length", num);
                        goto INSERTNUMBER;
                    }
                    // print detail.
                    int indexOfItem = num - 1;
                    Console.WriteLine("Do you want to Remove item {0} ?", player.Inventory[indexOfItem].item.name);
                    while (true)
                    {
                        Console.WriteLine("y = Confirm , n = Cancel");
                        ConsoleKey key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Y)
                        {
                            player.Inventory.RemoveAt(indexOfItem);
                            break;
                        }
                        else if (key == ConsoleKey.N)
                        {
                            break;
                        }
                    }
                }
                else if (read == ConsoleKey.E)
                {
                    if (player.Inventory.Count == 0)
                        continue;
                    INSERTNUMBER:
                    int num;
                    Console.Write("Insert number : ");
                    string str = Console.ReadLine();
                    if (Int32.TryParse(str, out num) == false)
                        goto INSERTNUMBER;
                    if (num > player.Inventory.Count || num <= 0)
                    {
                        Console.WriteLine("{0} is out of length", num);
                        goto INSERTNUMBER;
                    }
                    //confirm pickup item.
                    int indexOfItem = num - 1;
                    CONFIRM:
                    Console.WriteLine("Do you want to Use/Equip item {0} ?", player.Inventory[indexOfItem].item.name);
                    while (true)
                    {
                        Console.WriteLine("y = Confirm , n = Cancel");
                        ConsoleKey key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Y)
                        {
                            if (player.EquipItem(player.Inventory[indexOfItem].item) == true)
                                break;
                            else
                                goto CONFIRM;
                        }
                        else if (key == ConsoleKey.N)
                            break;
                    }
                }
                else if (read == ConsoleKey.Backspace)
                    break;
            }

        }
        /// <summary>
        /// create flow when select status.
        /// </summary>
        public void SelectStatus(Player player)
        {
            while (true)
            {
                player.UpdateStatus();
                DrawScreen.DrawStatusScreen(player);
                if (Console.ReadKey().Key == ConsoleKey.Backspace)
                    break;
            }
        }
        /// <summary>
        /// create flow when select equipment.
        /// </summary>
        public void SelectEquipment(Player player)
        {
            while (true)
            {
                EQUIPMENT:
                DrawScreen.DrawEquipmentScreen(player);
                ConsoleKey read = Console.ReadKey().Key;
                if (read == ConsoleKey.S)
                {
                    SELECTTYPE:
                    Console.WriteLine("Select Type : Weapons, Armors, Accessorys \n Type 'Exit' to exit.");
                    string type = Console.ReadLine().ToLower();
                    char c;
                    Item selectedItem = new Item();
                    //Weapon
                    if (type.Contains("wea"))
                    {
                        SELECTITEM:
                        Console.WriteLine("Select number : 1)Left hand, 2)Right hand");
                        Console.WriteLine("(C)Cancel");
                        c = Console.ReadKey().KeyChar;
                        if (c == '1')
                            selectedItem = player.equipment.weapons[0].GetCopy();
                        else if (c == '2')
                            selectedItem = player.equipment.weapons[1].GetCopy();
                        else if (c == 'c')
                            goto SELECTTYPE;
                        else
                            goto SELECTITEM;
                        if (player.equipment.FindIndexOfItemInEquipment(selectedItem) == -1)
                        {
                            Console.WriteLine("invalid item");
                            goto SELECTITEM;
                        }
                    }
                    //Armor
                    else if (type.Contains("arm"))
                    {
                        SELECTITEM:
                        int num;
                        Console.WriteLine("Select number : 1)Head, 2)Body, 3)L_hand, 4)R_hand, 5)L_leg, 6)R_leg ");
                        Console.WriteLine("(C)Cancel");
                        c = Console.ReadKey().KeyChar;
                        if (c == 'c')
                            goto SELECTTYPE;
                        if (Int32.TryParse(c.ToString(), out num) == false)
                            goto SELECTITEM;
                        else if (num > 6 || num < 1)
                            goto SELECTITEM;
                        else
                            selectedItem = player.equipment.armors[num - 1];
                        if (player.equipment.FindIndexOfItemInEquipment(selectedItem) == -1)
                        {
                            Console.WriteLine("invalid item");
                            goto SELECTITEM;
                        }
                    }
                    //Accessory
                    else if (type.Contains("acc"))
                    {
                        SELECTITEM:
                        int num;
                        Console.WriteLine("Select number : 1)Neck 2)Belt 3)Cape 4)L_ear 5)R_ear 6)Ring1 7)Ring2 8)Ring3");
                        Console.WriteLine("(C)Cancel");
                        c = Console.ReadKey().KeyChar;
                        if (c == 'c')
                            goto SELECTTYPE;
                        if (Int32.TryParse(c.ToString(), out num) == false)
                            goto SELECTITEM;
                        else if (num > 8 || num < 1)
                            goto SELECTITEM;
                        else
                            selectedItem = player.equipment.accessories[num - 1];
                        if (player.equipment.FindIndexOfItemInEquipment(selectedItem) == -1)
                        {
                            Console.WriteLine("invalid item");
                            goto SELECTITEM;
                        }
                    }
                    //exit
                    else if ("exit".Contains("ex"))
                        goto EQUIPMENT;
                    else
                        goto SELECTTYPE;
                    SELECTCOMMAND:
                    Console.WriteLine("(D)Detail of Item");
                    Console.WriteLine("(U)UnEquip Item");
                    c = Console.ReadKey().KeyChar;
                    if (c == 'd')
                    {
                        do
                            DrawScreen.DrawDetailOfItemScreen(selectedItem);
                        while (Console.ReadKey().Key != ConsoleKey.Backspace);
                        goto EQUIPMENT;
                    }
                    else if (c == 'u')
                    {
                        player.UnEquipItem(selectedItem);
                        goto EQUIPMENT;
                    }
                    else
                        goto SELECTCOMMAND;
                }
                else if (read == ConsoleKey.Backspace)
                    break;
            }
        }
        /// <summary>
        /// create flow when open chest.
        /// </summary>
        public void SelectOpenChest(Map map, Player player)
        {
            do
            {
                int chestId = map.FindObjIdNearPlayer("c", map.playerPosition);
                DrawScreen.DrawOpenChestScreen(map.listOfChest, chestId);
                ConsoleKey read = Console.ReadKey().Key;
                if (read == ConsoleKey.D)
                {
                    if (map.listOfChest[chestId].Count == 0)
                        continue;
                    INSERTNUMBER:
                    int num;
                    Console.Write("Insert number : ");
                    string str = Console.ReadLine();
                    if (Int32.TryParse(str, out num) == false)
                        goto INSERTNUMBER;
                    if (num > map.listOfChest[chestId].Count || num <= 0)
                    {
                        Console.WriteLine("{0} is out of length", num);
                        goto INSERTNUMBER;
                    }
                    // print detail.
                    int indexOfItem = num - 1;
                    while (true)
                    {
                        DrawScreen.DrawDetailOfItemScreen(map.listOfChest[chestId][indexOfItem].item);
                        if (Console.ReadKey().Key == ConsoleKey.Backspace)
                            break;
                    }
                }
                else if (read == ConsoleKey.P)
                {
                    if (map.listOfChest[chestId].Count == 0)
                        continue;
                    INSERTNUMBER:
                    int num;
                    Console.Write("Insert number : ");
                    string str = Console.ReadLine();
                    if (Int32.TryParse(str, out num) == false)
                        goto INSERTNUMBER;
                    if (num > map.listOfChest[chestId].Count || num <= 0)
                    {
                        Console.WriteLine("{0} is out of length", num);
                        goto INSERTNUMBER;
                    }
                    //confirm pickup item.
                    int indexOfItem = num - 1;
                    Console.WriteLine("Do you want to pick {0} ?", map.listOfChest[chestId][indexOfItem].item.name);
                    while (true)
                    {
                        Console.WriteLine("y = Confirm , n = Cancel");
                        ConsoleKey key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Y)
                        {
                            player.AddItem(map.listOfChest[chestId][indexOfItem].item.GetCopy(), map.listOfChest[chestId][indexOfItem].count);
                            map.listOfChest[chestId].RemoveAt(indexOfItem);
                            break;
                        }
                        else if (key == ConsoleKey.N)
                            break;
                    }
                }
                else if (read == ConsoleKey.Backspace)
                    break;
            }
            while (true);
        }
        /// <summary>
        /// create flow when open door.
        /// </summary>
        public void SelectOpenDoor()
        {
            while (true)
            {
                Console.WriteLine("Locked.");
                ConsoleKey read = Console.ReadKey().Key;
                break;
            }
        }

        /// <summary>
        /// create flow when Talk Npc.
        /// </summary>
        public void SelectTalkNPC(ref NPC npc, ref Player player)
        {
            while (true)
            {
                DrawScreen.DrawTalkWithNpc(npc.name);
                Console.WriteLine("NPC Gold : {0}", npc.gold);
                MENU:
                Console.WriteLine(npc.name + " : Can I help you? \n(1)Buy Item (2)Talk (BackSpace)Quit\t\t\t Gold : {0}\n", player.gold);
                ConsoleKey read = Console.ReadKey().Key;
                if (read == ConsoleKey.Backspace)
                    break;
                else if (read == ConsoleKey.D1)
                {
                    Console.WriteLine("Okay. Please take a look at my Inventory.");
                    for (int i = 0; i < npc.Inventory.Count; i++)
                    {
                        Console.WriteLine((i + 1) + ") " + npc.Inventory[i].item.name + " x" + npc.Inventory[i].count);
                    }
                    while (true)
                    {
                        int num;
                        Console.Write("Insert number : ");
                        string str = Console.ReadLine();
                        if (Int32.TryParse(str, out num) == false)
                            continue;
                        if (num > npc.Inventory.Count || num <= 0)
                        {
                            Console.WriteLine("{0} is out of length", num);
                            continue;
                        }
                        else
                        {
                            Console.WriteLine("How many do you want?");
                            string count = Console.ReadLine();
                            ItemInventory itemInv = npc.Sell(ref player, num - 1, Convert.ToInt32(count));
                            if (itemInv != null)
                            {
                                player.AddItem(itemInv.item, itemInv.count);
                                break;
                            }
                        }
                    }
                }
                else if (read == ConsoleKey.D2)
                {
                    Console.WriteLine();
                    Random rand = new Random();
                    // num = {0,1,2,3}
                    int num = rand.Next(0, 4);
                    if (num == 0 || num == 1)
                        Console.WriteLine("Chinnie : When I grow up. I want to be Game Developer.");
                    else if (num == 2)
                        Console.WriteLine("Chinnie : Hi Welcome to my shop. My name is Chinnawat Sirima");
                    else
                        Console.WriteLine("Chinnie : Have a good day");
                    goto MENU;
                }
            }
        }
        Player CreateCharacter(Player player)
        {
            string name;
            Console.Clear();
            Console.WriteLine("### Create New Character ###");
            Console.WriteLine("Name : ");
            Console.Write("# ");
            name = Console.ReadLine();
            Player.Gender gen = GetGender();
            Player.Race race = GetRace();
            return new Player(name, race, gen);
        }
        /// <summary>
        /// Create Get Gender Flow.
        /// </summary>
        /// <returns>Player.Gender</returns>
        Player.Gender GetGender()
        {
            Player.Gender gen = (Player.Gender)0;
            bool pass = false;
            do
            {
                Console.WriteLine("Select your Gender : \n (1) Man \n (2) Woman");
                Console.Write("# ");
                string input = Console.ReadLine();
                int num;
                if (Int32.TryParse(input, out num))
                {
                    if (num == 1 || num == 2)
                    {
                        pass = true;
                        gen = (Player.Gender)(num - 1);
                    }
                    else
                        Console.WriteLine(num + " doesn't exist.");
                }
                else
                {
                    Console.WriteLine(num + " doesn't exist.");
                }
            } while (!pass);
            return gen;
        }
        /// <summary>
        /// Create Get Race Flow.
        /// </summary>
        /// <returns>Player.Race</returns>
        Player.Race GetRace()
        {
            Player.Race race = (Player.Race)0;
            bool pass = false;
            do
            {
                Console.WriteLine("Select your Race : \n (1) Human \n (2) Elf");
                Console.Write("# ");
                string input = Console.ReadLine();
                int num;
                if (Int32.TryParse(input, out num))
                {
                    if (num == 1 || num == 2)
                    {
                        pass = true;
                        race = (Player.Race)(num - 1);
                    }
                    else
                        Console.WriteLine(num + " doesn't exist.");
                }
                else
                {
                    Console.WriteLine(num + " doesn't exist.");
                }
            } while (!pass);
            return race;
        }
    }
}
